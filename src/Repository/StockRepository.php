<?php

namespace App\Repository;

use App\Entity\Delivery;
use App\Entity\PreOrder;
use App\Entity\Stock;
use App\Interface\MoveProductionByStockInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Stock>
 *
 * @method Stock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stock[]    findAll()
 * @method Stock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stock::class);
    }

    public function save(Stock $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function createStock(MoveProductionByStockInterface $moveProductionByStock, bool $flush = false): void
    {
        $stock = (new Stock())
            ->setProduct($moveProductionByStock->getProduct())
            ->setQuantity($moveProductionByStock->getQuantity())
            ->setDateUpdate($moveProductionByStock->getDate())
        ;
        if($moveProductionByStock instanceof Delivery){
            $stock->addDelivery($moveProductionByStock);
        }

        if($moveProductionByStock instanceof PreOrder){
            $stock->addPreOrder($moveProductionByStock);
        }

        $this->getEntityManager()->persist($stock);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Stock $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByProductField($product): ?Stock
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.product = :val')
            ->setParameter('val', $product)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAllField():?array
    {
        return $this->createQueryBuilder('s')
            ->select('DISTINCT s.product')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param string|null $product
     * @param \DateTimeInterface|null $byDate
     * @return array|null Returns an array of Delivery objects
     */
    public function getCostPriceProduct(?string $product = null, ?\DateTimeInterface $byDate = null): ?array
    {

        $groupBy = ['s.product'];
        $selectFields = [
            's.product as name',
            'sum(d.price)/sum(s.quantity) as price',
            'sum(s.quantity) as quantity',
        ];

        $qb = $this->createQueryBuilder('s')
            ->select($selectFields)
            ->leftJoin('s.deliveries','d')
            ->leftJoin('s.preOrders','po')
            ->andWhere('s.product = :prod')
            ->groupBy(...$groupBy)
            ->setParameter('prod', $product)
        ;

        if ($byDate !== null) {
            $qb
                ->andWhere($qb->expr()->lte('s.date_update', ':date'))
                ->setParameter('date', $byDate);
        }

        if (!$product) {
            return null;
        }

        return $qb->getQuery()->getResult();
    }
}
