<?php

namespace App\DataFixtures;

use App\Entity\Delivery;
use App\Entity\PreOrder;
use App\Service\MoveToStockService;
use App\Service\PreOrderService;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    private array $entryDataArray = [
        'numDelivery' => ['1', 't-500', '12-TP-777', '12-TP-778', '12-TP-779', '12-TP-877', '12-TP-977', '12-TP-979'],
        'product' => ['Колбаса', 'Пармезан', 'Левый носок', 'Левый носок', 'Левый носок', 'Левый носок', 'Левый носок', 'Левый носок'],
        'quantity' => [300, 10, 100, 50, 77, 32, 94, 200],
        'priceDelivery' => [5000, 6000, 500, 300, 539, 176, 554, 1000],
        'dateDelivery' => ['2021-01-01', '2021-01-02', '2021-01-13', '2021-01-14', '2021-01-20', '2021-01-30', '2021-02-01', '2021-02-05']
    ];

    public function __construct(
        private MoveToStockService $moveToStockService,
        private PreOrderService $preOrderService
    )
    {}

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {

        for($i = 0; $i < count($this->entryDataArray['numDelivery']); $i++){
            $data = [];

            $data['numDelivery'] = $this->entryDataArray['numDelivery'][$i];
            $data['product'] = $this->entryDataArray['product'][$i];
            $data['quantity'] = $this->entryDataArray['quantity'][$i];
            $data['priceDelivery'] = $this->entryDataArray['priceDelivery'][$i];
            $data['dateDelivery'] = $this->entryDataArray['dateDelivery'][$i];

            $delivery = $this->createDelivery($data);
            $manager->persist($delivery);
            $this->moveToStockService->MoveProductsToStock($delivery);

        }

        $dataPreOrder = $this->preOrderService->getTotalPreOrderByFib('2021-01-13', '2021-02-5');

        for ($i = 0; $i < count($dataPreOrder); $i++){
            $preOrder = $this->createPreOrder($dataPreOrder[$i]);
            $manager->persist($preOrder);
            $this->moveToStockService->MoveProductsToStock($preOrder);
        }

        $manager->flush();

    }

    /**
     * @throws \Exception
     */
    public function createDelivery(array $data): Delivery
    {

        return (new Delivery())
            ->setNumDelivery($data['numDelivery'])
            ->setProduct($data['product'])
            ->setQuantity($data['quantity'])
            ->setPrice($data['priceDelivery'])
            ->setDateDelivery(new DateTimeImmutable($data['dateDelivery']))
        ;

    }

    /**
     * @throws \Exception
     */
    public function createPreOrder(array $data): PreOrder
    {

        return (new PreOrder())
            ->setProduct($data['product'])
            ->setQuantity($data['quantity'])
            ->setDate(new DateTimeImmutable($data['date']))
        ;

    }
}
