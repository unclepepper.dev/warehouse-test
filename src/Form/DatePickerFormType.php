<?php

namespace App\Form;

use App\Dto\ProductDto;
use App\Entity\Delivery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;

class DatePickerFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): FormBuilderInterface
    {
        $builder
            ->add('date', DateType::class, [
                'required' => false,
                ]
            )
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary px-3'
                ]
            ]);

        return $builder;
    }

//    public function configureOptions(OptionsResolver $resolver): void
//    {
//        $resolver->setDefaults([
//            'data_class' => ProductDto::class,
//        ]);
//    }

}