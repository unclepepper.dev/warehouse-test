<?php

namespace App\Service;

class MarkupService
{
    public function margin(?float $number, int $percent): float
    {
        if($number){
            $number_percent = $number / 100 * $percent;

            return round($number + $number_percent, 2);
        }
        return false;
    }
}