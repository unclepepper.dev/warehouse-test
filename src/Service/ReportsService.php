<?php

namespace App\Service;

use App\Dto\ProductDto;
use App\Repository\StockRepository;

class ReportsService
{

    public function __construct(
        private StockRepository $stockRepository,
    )
    {}

    public function getProduct(?string $product, ?\DateTime $date = null): ?ProductDto
    {

        $query = $this->stockRepository->getCostPriceProduct($product, $date);

        if($query){
            return new ProductDto(
                $query[0]['name'] ?? null,
                (int)$query[0]['quantity'] ?? null,
                round((float)$query[0]['price'], 2)  ?? null
            );

        }else{
            return  new ProductDto();
        }
    }

}