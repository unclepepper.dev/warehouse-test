<?php

namespace App\Service;

class PreOrderService
{

    public function getTotalPreOrderByFib(string $firstDate, ?string $lastDate = null): array
    {
        $arr = [];
        $numberDays = $this->getNumberOfDays($firstDate, $lastDate);
        $fib = $this->fib($numberDays);
        for($i = 0; $i < $numberDays; $i++){
            $arr[$i] = $this->getArrayPreOrder($fib[$i], 'Левый носок',$this->plusOneDay($i, $firstDate));
        }

        return $arr;
    }

    public function fib($n): array
    {
        $res = [0,1];
        for( $i=0; $i < ($n-2); $i++ ){
            $cur = $res[$i] + $res[$i+1];
            $res[] = $cur;
        }
        return $res;
    }

    public function getNumberOfDays(string $firstDate, ?string $lastDate = null): int
    {
        $lastDate = !$lastDate ? strtotime(date("Y-m-d")): $lastDate;
        $diff = ABS(strtotime($firstDate) - strtotime($lastDate));
        return floor($diff / 86400);
    }

    public function plusOneDay(int $count, string $date): string
    {

        return date('Y-m-d',strtotime("+$count day", strtotime($date)));

    }

    public function getArrayPreOrder(int $number, string $productName, $date): array
    {

        return ['product' => $productName, 'quantity' => -1 * $number, 'date' => $date];

    }

}