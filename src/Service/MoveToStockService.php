<?php

namespace App\Service;

use App\Interface\MoveProductionByStockInterface;
use App\Repository\StockRepository;

class MoveToStockService
{
    public function __construct(
        private StockRepository $stockRepository,
    )
    {}

    public function MoveProductsToStock(MoveProductionByStockInterface $delivery): void
    {

        $this->stockRepository->createStock($delivery,true);

    }

}