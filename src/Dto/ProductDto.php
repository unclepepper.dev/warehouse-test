<?php

namespace App\Dto;

use App\Service\MarkupService;

class ProductDto
{
    private const PRECENT = 30;

    private ?string $name;

    private ?int $quantity;

    private ?float $price;

    private ?float $marginPrice;


    public function __construct(?string $name = null,
                                ?int $quantity = null,
                                ?float $price = null)
    {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->marginPrice = (new MarkupService())->margin($price, self::PRECENT);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return ProductDto
     */
    public function setName(?string $name): ProductDto
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     * @return ProductDto
     */
    public function setQuantity(?int $quantity): ProductDto
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return ProductDto
     */
    public function setPrice(?float $price): ProductDto
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMarginPrice(): ?float
    {
        return $this->marginPrice;
    }

    /**
     * @param float|null $marginPrice
     * @return ProductDto
     */
    public function setMarginPrice(?float $marginPrice): ProductDto
    {
        $this->marginPrice = $marginPrice;
        return $this;
    }

}