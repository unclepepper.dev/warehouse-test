<?php

namespace App\Controller;

use App\Form\DatePickerFormType;
use App\Repository\StockRepository;
use App\Service\ReportsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    public function __construct(private StockRepository $stockRepository,
                                private ReportsService $reportsService)
    {}

    #[Route('/', name: 'app_index')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(DatePickerFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $date = $form->get('date')->getData();
            $products = $this->getArr($date);
        }else{
            $products = $this->getArr();
        }

        return $this->render('index/index.html.twig', [
            'products'=> $products,
            'datePickerForm' => $form->createView(),
        ]);
    }

    /**
     * @return array
     */
    public function getArr(?\DateTime $date = null): array
    {
        $stock = $this->stockRepository->findAllField();

        $products = array_map(fn($prod) => $this->reportsService->getProduct($prod['product'], $date), $stock);
        return array_filter($products, fn($prod) => $prod->getName() !== null);
    }
}
