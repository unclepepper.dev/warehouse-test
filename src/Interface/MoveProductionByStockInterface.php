<?php

namespace App\Interface;

use App\Entity\Stock;
use App\Service\MoveToStockService;

interface MoveProductionByStockInterface
{
    public function getProduct(): string;
    public function getQuantity(): int;
    public function getDate(): \DateTimeImmutable;
    public function getStock(): ?Stock;
}