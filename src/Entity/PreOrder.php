<?php

namespace App\Entity;

use App\Interface\MoveProductionByStockInterface;
use App\Repository\PreOrderRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PreOrderRepository::class)]
class PreOrder implements MoveProductionByStockInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(length: 255)]
    private string $product;

    #[ORM\Column]
    private int $quantity = 0;

    #[ORM\Column]
    private \DateTimeImmutable $date;

    #[ORM\ManyToOne(inversedBy: 'preOrders')]
    private ?Stock $stock = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PreOrder
     */
    public function setId(int $id): PreOrder
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @param string $product
     * @return PreOrder
     */
    public function setProduct(string $product): PreOrder
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return PreOrder
     */
    public function setQuantity(int $quantity): PreOrder
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @param \DateTimeImmutable $date
     * @return PreOrder
     */
    public function setDate(\DateTimeImmutable $date): PreOrder
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Stock|null
     */
    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    /**
     * @param Stock|null $stock
     * @return PreOrder
     */
    public function setStock(?Stock $stock): PreOrder
    {
        $this->stock = $stock;
        return $this;
    }

}
