<?php

namespace App\Entity;

use App\Interface\MoveProductionByStockInterface;
use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StockRepository::class)]
class Stock
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(length: 255)]
    private string $product;

    #[ORM\Column]
    private int $quantity = 0;

    #[ORM\OneToMany(mappedBy: 'stock', targetEntity: Delivery::class)]
    private Collection $deliveries;

    #[ORM\Column(type: Types::DATE_IMMUTABLE)]
    private \DateTimeImmutable $date_update;

    #[ORM\OneToMany(mappedBy: 'stock', targetEntity: PreOrder::class)]
    private Collection $preOrders;

    public function __construct()
    {
        $this->deliveries = new ArrayCollection();
        $this->preOrders = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Stock
     */
    public function setId(int $id): Stock
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @param string $product
     * @return Stock
     */
    public function setProduct(string $product): Stock
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Stock
     */
    public function setQuantity(int $quantity): Stock
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Collection<int, Delivery>
     */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    public function __toString()
    {
        return $this->product;
    }

    public function addDelivery(Delivery $delivery): ?self
    {
        if (!$this->deliveries->contains($delivery)) {
            $this->deliveries->add($delivery);
            $delivery->setStock($this);
        }

        return $this;
    }

    public function removeDelivery(Delivery $delivery): self
    {
        if ($this->deliveries->removeElement($delivery)) {
            // set the owning side to null (unless already changed)
            if ($delivery->getStock() === $this) {
                $delivery->setStock(null);
            }
        }

        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDateUpdate(): \DateTimeImmutable
    {
        return $this->date_update;
    }

    /**
     * @param \DateTimeImmutable $date_update
     * @return Stock
     */
    public function setDateUpdate(\DateTimeImmutable $date_update): Stock
    {
        $this->date_update = $date_update;
        return $this;
    }

    /**
     * @return Collection<int, PreOrder>
     */
    public function getPreOrders(): Collection
    {
        return $this->preOrders;
    }

    public function addPreOrder(PreOrder $preOrder): self
    {
        if (!$this->preOrders->contains($preOrder)) {
            $this->preOrders->add($preOrder);
            $preOrder->setStock($this);
        }

        return $this;
    }

    public function removePreOrder(PreOrder $preOrder): self
    {
        if ($this->preOrders->removeElement($preOrder)) {
            // set the owning side to null (unless already changed)
            if ($preOrder->getStock() === $this) {
                $preOrder->setStock(null);
            }
        }

        return $this;
    }

}
