<?php

namespace App\Entity;

use App\Interface\MoveProductionByStockInterface;
use App\Repository\DeliveryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: DeliveryRepository::class)]
#[UniqueEntity('num_delivery')]
class Delivery implements MoveProductionByStockInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(length: 255, unique: true)]
    private string $num_delivery;

    #[ORM\Column(length: 255)]
    private string $product;
    #[ORM\Column]
    private int $quantity = 0;

    #[ORM\Column]
    private float $price = 0;

    #[ORM\Column(type: Types::DATE_IMMUTABLE)]
    private \DateTimeImmutable $date_delivery;

    #[ORM\ManyToOne(inversedBy: 'deliveries')]
    private ?Stock $stock = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Delivery
     */
    public function setId(int $id): Delivery
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumDelivery(): string
    {
        return $this->num_delivery;
    }

    /**
     * @param string $num_delivery
     * @return Delivery
     */
    public function setNumDelivery(string $num_delivery): Delivery
    {
        $this->num_delivery = $num_delivery;
        return $this;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @param string $product
     * @return Delivery
     */
    public function setProduct(string $product): Delivery
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Delivery
     */
    public function setQuantity(int $quantity): Delivery
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Delivery
     */
    public function setPrice(float $price): Delivery
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date_delivery;
    }

    /**
     * @param \DateTimeImmutable $date_delivery
     * @return Delivery
     */
    public function setDateDelivery(\DateTimeImmutable $date_delivery): Delivery
    {
        $this->date_delivery = $date_delivery;
        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function __toString()
    {
        return $this->product;
    }

}
