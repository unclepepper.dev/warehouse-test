# warehouse-test

![title](warehouse.png)

### Для запуска приложения выполнить:

*  ```mv .env.example .env```
*  ```docker-compose up -d --build```
*  ```docker-compose exec app composer install```

### Для наката миграций и создания фикстур в БД выполнить:

*  ```docker-compose exec app bin/console doctrine:database:create```
*  ```docker-compose exec app bin/console doctrine:migrations:migrate```
*  ```docker-compose exec app bin/console doctrine:fixtures:load```

#### Приложение доступно на:

- [localhost](http://localhost)

#### БД доступна на:

- [localhost:8082](http://localhost:8082)
