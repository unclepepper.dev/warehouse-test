FROM composer:latest as composer
FROM php:8.0-fpm-alpine as php

RUN set -xe \
        && apk add --no-cache \
           shadow \
           libzip-dev \
           libintl \
           icu \
           icu-dev \
           curl \
           bash\
           libmcrypt \
           libmcrypt-dev \
           libxml2-dev \
           freetype \
           freetype-dev \
           libpng \
           libpng-dev \
           libjpeg-turbo \
           libjpeg-turbo-dev \
           postgresql-dev \
           pcre-dev \
           git \
           g++ \
           make \
           autoconf \
           openssh \
           util-linux-dev \
           libuuid \
           sqlite-dev \
           libxslt-dev \
    && docker-php-ext-configure gd \
                  --with-freetype=/usr/include/ \
                  --with-jpeg=/usr/include/


RUN docker-php-ext-install pdo_mysql\
    pdo_sqlite

RUN docker-php-ext-install -j$(nproc) \
    zip \
    gd \
    intl \
    soap \
    opcache \
    pcntl \
    exif \
    pdo_pgsql \
    xsl

RUN apk add --update linux-headers

RUN pecl update-channels && pecl install xdebug && \
    docker-php-ext-enable xdebug \
    && echo  xdebug.mode=debug >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

#ENV XDEBUG_CONFIG="start_with_request=yes"
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1

COPY docker/dev/app/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY docker/dev/app/error_reporting.ini /usr/local/etc/php/conf.d/error_reporting.ini

COPY --from=composer /usr/bin/composer /usr/local/bin/composer
RUN /usr/local/bin/composer self-update

ARG UID
ARG GID
ENV TARGET_UID ${UID:-1000}
ENV TARGET_GID ${GID:-1000}


RUN usermod -u ${TARGET_UID} www-data && groupmod -g ${TARGET_GID} www-data
RUN mkdir -p /var/www/php/vendor && mkdir -p /var/www/php/var/cache && chown -R www-data:www-data /var/www/php
RUN mkdir -p /var/sf/vendor && mkdir -p /var/sf/var && chown -R www-data:www-data /var/sf

WORKDIR /var/www/app

USER ${TARGET_UID}:${TARGET_GID}